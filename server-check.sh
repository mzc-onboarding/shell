#!/bin/bash

# Check CPU usage
cpu_usage=$(top -b -n2 -p 1 | fgrep "Cpu(s)" | tail -1 | awk -F'id,' -v prefix="$prefix" '{ split($1, vs, ","); v=vs[length(vs)]; sub("%", "", v); printf "%s%.1f%%\n", prefix, 100 - v }')
if [[ $cpu_usage > 70 ]]
then
    cpu_status="High CPU usage: $cpu_usage"
else
    cpu_status="CPU usage: $cpu_usage"
fi

# Check memory usage
memory_usage=$(free -m | awk 'NR==2{printf "%.2f%%\t\t", $3*100/$2 }')
if [[ $memory_usage > 70 ]]
then
    memory_status="High memory usage: $memory_usage"
else
    memory_status="Memory usage: $memory_usage"
fi

# Check disk usage
disk_usage=$(df -h | awk '$NF=="/"{printf "%s\t\t", $5}')
if [[ $disk_usage > 70 ]]
then
    disk_status="High disk usage: $disk_usage"
else
    disk_status="Disk usage: $disk_usage"
fi

# Check for updates
updates=$(apt-get --just-print upgrade | grep "upgraded," | awk -F ', ' '{print $2}')
if [[ -z $updates ]]
then
    updates_status="No updates available"
else
    updates_status="$updates updates available"
fi

# Check running processes
processes=$(ps -A | wc -l)
processes_status="Number of running processes: $processes"

# Check if swap is enabled
swap=$(swapon -s)
if [ -n "$swap" ]; then
    swap_status="Swap is enabled"
else
    swap_status="Swap is disabled"
fi

# Check system uptime
uptime=$(uptime | awk '{print $3,$4}' | sed 's/,//')
uptime_status="System uptime: $uptime"

# Get hostname
hostname=$(hostname)

# Send notification to Slack
curl -X POST -H 'Content-type: application/json' --data "{\"text\":\"Server report for $hostname:\n$cpu_status\n$memory_status\n$disk_status\n$updates_status\n$processes_status\n$swap_status\n$uptime_status\"}" https://hooks.slack.com/services/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
